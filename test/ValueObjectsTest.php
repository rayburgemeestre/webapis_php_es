<?php
class ValueObjectsTest extends PHPUnit_Framework_TestCase
{
    public function testGetIndex()
    {
        $filter = new \CppSe\Slack\Filters\ValueObjectFilter();
        $data = ['foo' => 'bar', 'baz' => ['ban' => 'boo']];
        $this->assertEquals('bar', $filter->getIndex('', $data, 'foo'));
        $this->assertEquals('boo', $filter->getIndex('', $data, ['baz', 'ban']));
    }

    public function testMappingFromSlack()
    {
        $slackUser = array(
            'id' => 'U024QAD1F',
            'name' => 'rayburgemeestre',
            'deleted' => false,
            'status' => NULL,
            'color' => 'e7392d',
            'real_name' => 'Ray Burgemeestre',
            'tz' => 'Europe/Amsterdam',
            'tz_label' => 'Central European Time',
            'tz_offset' => 3600,
            'profile' =>
                array (
                    'image_24' => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_24.jpg',
                    'image_32' => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_32.jpg',
                    'image_48' => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_48.jpg',
                    'image_72' => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_72.jpg',
                    'image_192' => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_192.jpg',
                    'image_original' => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_original.jpg',
                    'first_name' => 'Ray',
                    'last_name' => 'Burgemeestre',
                    'skype' => 'ray.burgemeestre',
                    'title' => 'Software engineer',
                    'real_name' => 'Ray Burgemeestre',
                    'real_name_normalized' => 'Ray Burgemeestre',
                    'email' => 'ray.burgemeestre@autotrack.nl',
                ),
            'is_admin' => true,
            'is_owner' => false,
            'is_primary_owner' => false,
            'is_restricted' => false,
            'is_ultra_restricted' => false,
            'is_bot' => false,
            'has_files' => true,
        );
        $filter = new \CppSe\Slack\Filters\ValueObjectFilter();
        $member = current($filter->filter(['members' => [$slackUser]]) );

        $this->assertEquals($slackUser['id'],                  $member->id());
        $this->assertEquals($slackUser['name'],                $member->name());
        $this->assertEquals($slackUser['deleted'],             $member->deleted());
        $this->assertEquals($slackUser['color'],               $member->color());
        $this->assertEquals($slackUser['profile']['image_32'], $member->avatar());
        $this->assertEquals($slackUser['is_bot'],              $member->isBot());
    }

    public function testMappingFromES()
    {
        $slackUser =  array (
            '_index' => 'slack_index',
            '_type' => 'member_type',
            '_id' => 'U024QAD1F',
            '_score' => 1,
            'fields' =>
                array (
                    'profile.image_32' =>
                        array (
                            0 => 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2014-01-13/2160353789_32.jpg',
                        ),
                    'id' =>
                        array (
                            0 => 'U024QAD1F',
                        ),
                    'color' =>
                        array (
                            0 => 'e7392d',
                        ),
                    'name' =>
                        array (
                            0 => 'rayburgemeestre',
                        ),
                ),
        );
        $filter = new \CppSe\Slack\Filters\ValueObjectFilter(\CppSe\Slack\Filters\ValueObjectFilter::SourceElasticSearch);
        $member = current($filter->filter([$slackUser]) );

        $this->assertEquals($slackUser['fields']['id'][0],               $member->id());
        $this->assertEquals($slackUser['fields']['name'][0],             $member->name());
        $this->assertEquals($slackUser['fields']['color'][0],            $member->color());
        $this->assertEquals($slackUser['fields']['profile.image_32'][0], $member->avatar());
    }
}
