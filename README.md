# Prerequisites

Install PHP

Install Composer and run it

    shell$ curl -sS https://getcomposer.org/installer | php
    shell$ php composer.phar install

Install ElasticSearch on Debian based system:

    shell$ wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.4.4.deb
    shell$ dpkg -i elasticsearch-1.4.4.deb  # and follow instructions

Install EleasticSearch on OSX, via brew:

    shell$ brew update elasticsearch
    ...
    shell$ brew info elasticsearch
    elasticsearch: stable 1.4.4, HEAD
    http://www.elasticsearch.org
    Not installed
    ...
    shell$ brew install elasticsearch

Follow instructions... (differs per platform)

# Start demo applications

Start a webserver

    shell$ php -S 127.0.0.1:8080 -t public/
    PHP 5.4.30 Development Server started at Sun Mar 22 10:57:53 2015
    Listening on http://127.0.0.1:8080
    Document root is /projects/slack/public
    Press Ctrl-C to quit.

Open in your browser

    http://localhost:8080/


# ElasticSearch Frontend

Install HEAD Plugin:

    trigen@Rays-MacBook-Pro.local:/usr/local/opt[master]> elasticsearch/bin/plugin -install mobz/elasticsearch-head
    -> Installing mobz/elasticsearch-head...
    Trying https://github.com/mobz/elasticsearch-head/archive/master.zip...
    Downloading .......... (...) DONE
    Installed mobz/elasticsearch-head into /usr/local/var/lib/elasticsearch/plugins/head
    Identified as a _site plugin, moving to _site structure ...

Open in your browser

    http://localhost:9200/_plugin/HEAD

You can easily browse and perform searches with a web GUI.

