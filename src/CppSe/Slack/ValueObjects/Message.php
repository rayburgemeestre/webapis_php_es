<?php
namespace CppSe\Slack\ValueObjects;

class Message extends \CppSe\DDD\ValueObject implements SlackValueObject
{
    const classname = '\CppSe\Slack\ValueObjects\Message';

    public function __construct($timestamp, $channel, $user, $text, $jsonSource)
    {
        $this->bind($this, func_get_args());
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['_source', 'id']}
     * @return null
     */
    public function id($value = self::undefined)
    {
        return $this->timestamp($value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['_source', 'ts']}
     * @Filter{Slack, 'ts'}
     * @return null
     */
    public function timestamp($value = self::undefined)
    {
        return $this->accessor('timestamp', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['_source', 'channel']}
     * @return null
     */
    public function channel($value = self::undefined)
    {
        return $this->accessor('channel', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['_source', 'user']}
     * @return null
     */
    public function user($value = self::undefined)
    {
        return $this->accessor('user', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['_source', 'text']}
     * @return null
     */
    public function text($value = self::undefined)
    {
        return $this->accessor('text', $value);
    }

    public function jsonSource($value = self::undefined)
    {
        return $this->accessor('jsonSource', $value);
    }
}
