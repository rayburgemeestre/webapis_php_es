<?php
namespace CppSe\Slack\ValueObjects;
interface SlackValueObject
{
    public function id($value = \CppSe\DDD\ValueObject::undefined);
    public function jsonSource($value = \CppSe\DDD\ValueObject::undefined);
}
