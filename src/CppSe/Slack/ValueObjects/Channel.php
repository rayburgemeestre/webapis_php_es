<?php
namespace CppSe\Slack\ValueObjects;

class Channel extends \CppSe\DDD\ValueObject implements SlackValueObject
{
    const classname = '\CppSe\Slack\ValueObjects\Channel';

    public function __construct($id, $name, $jsonSource)
    {
        $this->bind($this, func_get_args());
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'id', 0]}
     * @return null
     */
    public function id($value = self::undefined)
    {
        return $this->accessor('id', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'name', 0]}
     * @return null
     */
    public function name($value = self::undefined)
    {
        return $this->accessor('name', $value);
    }

    public function jsonSource($value = self::undefined)
    {
        return $this->accessor('jsonSource', $value);
    }
}
