<?php
namespace CppSe\Slack\ValueObjects;

class Member extends \CppSe\DDD\ValueObject implements SlackValueObject
{
    const classname = '\CppSe\Slack\ValueObjects\Member';

    public function __construct($id, $name, $deleted, $color, $avatar, $isBot, $jsonSource)
    {
        $this->bind($this, func_get_args());
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'id', 0]}
     * @Filter{Slack, 'id'}
     * @return null
     */
    public function id ($value = self::undefined)
    {
        return $this->accessor('id', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'name', 0]}
     * @return null
     */
    public function name ($value = self::undefined)
    {
        return $this->accessor('name', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'deleted', 0]}
     * @return null
     */
    public function deleted ($value = self::undefined)
    {
        return $this->accessor('deleted', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'color', 0]}
     * @return null
     */
    public function color ($value = self::undefined)
    {
        return $this->accessor('color', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'profile.image_32', 0]}
     * @Filter{Slack, ['profile', 'image_32']}
     * @return null
     */
    public function avatar ($value = self::undefined)
    {
        return $this->accessor('avatar', $value);
    }

    /**
     * @param string $value
     * @Filter{ElasticSearch, ['fields', 'is_bot', 0]}
     * @Filter{Slack, 'is_bot'}
     * @return null
     */
    public function isBot ($value = self::undefined)
    {
        return $this->accessor('isBot', $value);
    }

    public function jsonSource ($value = self::undefined)
    {
        return $this->accessor('jsonSource', $value);
    }
}
