<?php
namespace CppSe\Slack;

use CppSe\Slack\Filters\ValueObjectFilter;

class SlackSearchService
{
    private $elasticSearchClient = null;
    private $filter = null;

    public function __construct(\ElasticSearch\Client $client)
    {
        $this->elasticSearchClient = $client;
        $this->filter = new ValueObjectFilter(ValueObjectFilter::SourceElasticSearch);
    }

    public function users()
    {
        $users = array();
        $queryResponse = $this->elasticSearchClient->search([
                'index' => 'slack_index',
                'type' => 'member_type',
                'size' => 100,
                'body' => [
                    'fields' => [
                        'id', 'name', 'color', 'profile.image_32'
                    ]
                ]
            ]);

        /**@var \CppSe\Slack\ValueObjects\Member $user */
        foreach ($this->filter->filter($queryResponse['hits']['hits']) as $user) {
            $users[ $user->id() ] = $user;
        }
        return $users;
    }
    public function channels()
    {
        $channels = array();
        $queryResponse = $this->elasticSearchClient->search([
                'index' => 'slack_index',
                'type' => 'channel_type',
                'size' => 100,
                'body' => [
                    'fields' => [
                        'id', 'name'
                    ]
                ]
            ]);

        foreach ($this->filter->filter($queryResponse['hits']['hits']) as $channel) {
            $channels[ $channel->id() ] = $channel;
        }
        return $channels;
    }

    public function messages($currentQuery, $channelSelected)
    {
        $filter = [];
        $query = [];

        if (!empty($channelSelected))
            $filter['term']['channel']  = $this->analyzedChannel($channelSelected);

        if (!empty($currentQuery))
            $query['match']['text'] = $currentQuery;

        $messages = $this->elasticSearchClient->search([
                'index' => 'slack_index',
                'type' => 'message_type',
                'size' => 100,
                'body' => [
                    'query' => [
                        'filtered' => [
                            'filter' => $filter,
                            'query' => $query
                        ]
                    ],
                    'highlight' => [
                        'pre_tags' => ['@@highlight_begin@@'],
                        'post_tags' => ['@@highlight_end@@'],
                        'fields' => [
                            'text' => [
                                'fragment_size' => 80,
                                'number_of_fragments' => 3,
                            ]
                        ]
                    ]
                ],
            ]);
        return $this->filter->filter($messages['hits']['hits']);
    }

    public function messages_before($analyzedChannelCode, $timestampSource)
    {
        $messages = $this->elasticSearchClient->search([
                'index' => 'slack_index',
                'type'  => 'message_type',
                'size'  => 3,
                'body'  => [
                    'query' => [
                        'filtered' => [
                            'filter' => ['term' => [ 'channel' => $analyzedChannelCode ]],
                            'query' => ['range' => [ 'ts' => [ 'lt' => $timestampSource]]]
                        ],
                    ],
                    'sort' => ['ts' => [ 'order' => 'desc']]
                ],
            ]);
        return $this->filter->filter($messages['hits']['hits']);
    }

    public function analyzedChannel($channelSelected)
    {
        if (empty($channelSelected))
            return false;

        // Convert Channel by default analyzer
        $elasticSearchClient = \CppSe\Factory\ElasticSearch::create();
        $analyzed = $elasticSearchClient->indices()->analyze(['body' => $channelSelected])['tokens'];
        // "count($analyzed) > 1" cannot happen with my current use case channels always have a code that's a single token.
        $analyzed = current($analyzed)['token'];
        return $analyzed;
    }
}
