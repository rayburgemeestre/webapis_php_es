<?php
namespace CppSe\Slack\Filters;

class JsonDecodeFilter implements \CppSe\Utils\ReturnFilter
{
    public function __construct()
    {
    }

    public function filter($input)
    {
        $output = json_decode($input, true);

        if ($output['ok'] !== true)
            throw new \RuntimeException('slack api says ok === false in json output');

        return $output;
    }
}
