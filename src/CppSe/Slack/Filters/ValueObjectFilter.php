<?php
namespace CppSe\Slack\Filters;

use CppSe\Slack\ValueObjects\Channel;
use CppSe\Slack\ValueObjects\Member;
use CppSe\Slack\ValueObjects\Message;

class ValueObjectFilter implements \CppSe\Utils\ReturnFilter
{
    const SourceSlackAPI = 'Slack';
    const SourceElasticSearch = 'ElasticSearch';

    private $source = null;

    public function __construct($source = self::SourceSlackAPI)
    {
        $this->source = $source;
    }

    public function filter($input)
    {
        switch ($this->source) {
            case self::SourceElasticSearch:
                if (!isset($input['0']) && !isset($input[0]['_type']))
                    return array();
                switch ($input[0]['_type']) {
                    case 'member_type':
                        return $this->filterMembers($input);
                    case 'channel_type':
                        return $this->filterChannels($input);
                    case 'message_type':
                        return $this->filterMessages($input);
                }
                break;
            case self::SourceSlackAPI:
                if (isset($input['members']))
                    return $this->filterMembers($input['members']);
                if (isset($input['channels']))
                    return $this->filterChannels($input['channels']);
                if (isset($input['messages']))
                    return $this->filterMessages($input['messages']);
                break;
        }

        return array();
    }

    /**
     * @param array $data
     * @param string|array $index
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function getIndex($className, array $data, $index)
    {
        if (class_exists($className)) {
            $method = (new \ReflectionClass($className))->getMethod($index);
            $matches = array();
            $hints = array();
            if (preg_match_all('/Filter{([^,]*)\s?,\s?(.*)}/', $method->getDocComment(), $matches)) {
                foreach ($matches[1] as $indx => $match) {
                    $hints[ $match ] = json_decode(str_replace("'", '"', $matches[2][$indx]), true);
                }
                if (isset($hints[$this->source])) {
                    $index = $hints[$this->source];
                }
            }
        }

        if (!(is_string($index) || is_array($index))) {
            throw new \InvalidArgumentException('$index must either be a string or array');
        }

        if (is_string($index)) {
            if (!isset($data[$index]))
                return null;

            return $data[$index];
        }

        $copy = $data;
        foreach ($index as $indx) {
            if (!isset($copy[$indx]))
                return null;

            $copy = $copy[$indx];
        }
        return $copy;
    }

    /**
     * @param $input
     * @param $objects
     * @return array
     */
    private function filterMembers($input)
    {
        $objects = [];

        foreach ($input as $member) {
            $objects[] = new Member(
                (string) $this->getIndex(Member::classname, $member, 'id'),
                (string) $this->getIndex(Member::classname, $member, 'name'),
                (bool)   $this->getIndex(Member::classname, $member, 'deleted'),
                (string) $this->getIndex(Member::classname, $member, 'color'),
                (string) $this->getIndex(Member::classname, $member, 'avatar'),
                (bool)   $this->getIndex(Member::classname, $member, 'isBot'),
                $member
            );
        }

        return $objects;
    }

    private function filterChannels($input)
    {
        $objects = [];
        foreach ($input as $channel) {
            $objects[] = new Channel(
                (string) $this->getIndex(Channel::classname, $channel, 'id'),
                (string) $this->getIndex(Channel::classname, $channel, 'name'),
                $channel
            );
        }
        return $objects;
    }

    private function filterMessages($input)
    {
        $objects = [];
        foreach ($input as $message) {
            $objects[] = new Message(
                (string) $this->getIndex(Message::classname, $message, 'timestamp'),
                (string) $this->getIndex(Message::classname, $message, 'channel'),
                (string) $this->getIndex(Message::classname, $message, 'user'),
                (string) $this->getIndex(Message::classname, $message, 'text'),
                $message
            );
        }
        return $objects;
    }
}
