<?php

namespace CppSe\Slack\API;

interface SlackApiInterface
{
    public function setAccessToken($token);

    public function get($url, array $params = array());

    public function defaultParams();
}
