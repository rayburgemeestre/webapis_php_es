<?php

namespace CppSe\Slack\API;

class SlackApi implements SlackApiInterface
{
    private $clientId = null;
    private $clientSecret = null;
    private $accessToken = null;

    public function __construct($clientId, $clientSecret)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function accessToken()
    {
        return $this->accessToken;
    }

    public function setAccessToken($token)
    {
        $this->accessToken = $token;
    }

    public function get($url, array $params = array())
    {
        $params = array_merge($this->defaultParams(), $params);
        $url = $url . '?' . http_build_query($params, '', '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function defaultParams()
    {
        $params = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
        ];
        if ($this->accessToken)
            $params['token'] = $this->accessToken;
        return $params;
    }
}
