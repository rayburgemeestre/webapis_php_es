<?php
namespace CppSe\Slack;

use Elasticsearch\Client;

class ElasticSearchSynchronizer
{
    private $verbose = false;
    private $slackRepository = null;
    private $elasticSearchClient = null;

    public function __construct(SlackRepository $slackRepository, Client $elasticSearchClient)
    {
        $this->slackRepository = $slackRepository;
        $this->elasticSearchClient = $elasticSearchClient;
    }

    public function setVerbose($value)
    {
        $this->verbose = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function synchronizeMembers()
    {
        foreach ($this->slackRepository->members() as $member)
            $this->sync('slack_index', 'member_type', $member);
    }
    public function synchronizeChannels()
    {
        foreach ($this->slackRepository->channels() as $channel)
            $this->sync('slack_index', 'channel_type', $channel);
    }

    /**
     * @param array $channelsToProcess
     */
    public function synchronizeChannelsHistory(array $channelsToProcess)
    {
        /**@var \CppSe\Slack\ValueObjects\Channel[] $channels*/
        $channels = [];
        foreach ($this->slackRepository->channels() as $channel)
            if ($channel instanceof \CppSe\Slack\ValueObjects\Channel)
                if (in_array('#' . $channel->name(), $channelsToProcess))
                    $channels[$channel->name()] = $channel;

        foreach ($channels as $channel)
            foreach ($this->slackRepository->messages($channel, 10000) as $message)
                $this->sync('slack_index', 'message_type', $message, ['channel' => $channel->id()]);
    }

    private function sync(
        $index,
        $type,
        \CppSe\Slack\ValueObjects\SlackValueObject $object,
        array $extraFilters = array()
    ){
        $params = ['index' => $index, 'type' => $type, 'id' => $object->id()];
        try {
            $this->elasticSearchClient->get($params);
        }
        catch (\Elasticsearch\Common\Exceptions\Missing404Exception $ex) {
            $ret = $this->elasticSearchClient->index(array_merge($params, ['body' => array_merge($object->jsonSource(), $extraFilters)]));

            if ($this->verbose)
                printf("indexing %s %s %s V%d created=%s<br/>\n", $index, $type, $ret['_id'], $ret['_version'], $ret['created'] ?'true':'false');
        }
    }
}
