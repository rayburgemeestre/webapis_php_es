<?php

namespace CppSe\Slack;

use CppSe\Slack\Config\SlackApiUrls;
use CppSe\Slack\ValueObjects\Channel;

class SlackRepository
{
    /**
     * @var API\SlackApiInterface
     */
    private $api = null;

    /**
     * @var \CppSe\Utils\ReturnFilter[]
     */
    private $returnFilters = array();

    public function __construct(API\SlackApiInterface $api)
    {
        $this->api = $api;

        $this->addReturnFilter(new Filters\JsonDecodeFilter());
        $this->addReturnFilter(new Filters\ValueObjectFilter());
    }

    public function addReturnFilter(\CppSe\Utils\ReturnFilter $returnFilter)
    {
        $this->returnFilters[] = $returnFilter;
    }

    /**
     * @return Channel[]
     * @throws \RuntimeException
     */
    public function channels()
    {
        $result = $this->api->get(SlackApiUrls::channelsListUrl, array('pretty' => true));

        try {
            $filteredResult = $result;
            foreach ($this->returnFilters as $returnFilter) {
                $filteredResult = $returnFilter->filter($filteredResult);
            }
            return $filteredResult;
        }
        catch (\RuntimeException $ex) {
            throw new \RuntimeException('fetching channels.list failed ' . print_r($result, 1));
        }
    }
    public function members()
    {
        $result = $this->api->get(SlackApiUrls::usersListUrl, array('pretty' => true));

        try {
            $filteredResult = $result;
            foreach ($this->returnFilters as $returnFilter) {
                $filteredResult = $returnFilter->filter($filteredResult);
            }

            return $filteredResult;
        }
        catch (\RuntimeException $ex) {
            throw new \RuntimeException('fetching users.list failed ' . print_r($result, 1));
        }
    }

    public function messages(Channel $channel, $maxNumberMessages)
    {
        $params = [
            'channel' => $channel->id(),
            'count'   => (int)$maxNumberMessages,
            'pretty'  => true
        ];
        $result = $this->api->get(SlackApiUrls::channelsHistoryUrl, $params);

        try {
            $filteredResult = $result;
            foreach ($this->returnFilters as $returnFilter) {
                $filteredResult = $returnFilter->filter($filteredResult);
            }

            return $filteredResult;
        }
        catch (\RuntimeException $ex) {
            throw new \RuntimeException('fetching channels.history failed ' . print_r($result, 1));
        }
    }
}

