<?php
namespace CppSe\Slack\Config;

class SlackApiUrls
{
    const authorizeUrl   = 'https://slack.com/oauth/authorize';
    const accessTokenUrl = 'https://slack.com/api/oauth.access';

    const channelsListUrl = 'https://slack.com/api/channels.list';
    const channelsHistoryUrl = 'https://slack.com/api/channels.history';
    const usersListUrl = 'https://slack.com/api/users.list';
}
