<?php

namespace CppSe\Slack;

class SlackOAuthService
{
    /**
     * @var string|null
     */
    private $accessToken = null;
    /**
     * @var API\SlackApiInterface
     */
    private $api;

    public function __construct(API\SlackApiInterface $api)
    {
        $this->api = $api;
    }

    public function run(array $getParams = array())
    {
        $isRequestTokenAvailable = isset($getParams['code']);
        $isAccessTokenAvailable = isset($getParams['access']);

        if ($isAccessTokenAvailable) {
            $this->accessToken = $getParams['access'];
            $this->api->setAccessToken($getParams['access']);
            return;
        }

        if ($isRequestTokenAvailable) {
            $this->getAccessToken($getParams);
            return;
        }

        $this->getRequestToken();
    }

    public function getRedirectURI()
    {
        $url = $_SERVER['REQUEST_URI'];
        // strip get params
        $url = substr($url, 0, strrpos($url, '?') !== false ? strrpos($url, '?') : strlen($url));
        // prefix domain
        $url = $_SERVER['HTTP_HOST'] . $url;
        // prefix protocol
        $isHttps = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off');
        $url = ($isHttps ? 'https' : 'http') . '://' . $url;
        return $url;
    }


    /**
     * @param array $getParams
     */
    private function getAccessToken(array $getParams)
    {
        $result = $this->api->get(
            Config\SlackApiUrls::accessTokenUrl,
            [
                'grant_type' => 'authorization_code',
                'redirect_uri' => $this->getRedirectURI(),
                'code' => $getParams['code']
            ]
        );

        $access = json_decode($result);
        $accessToken = $access->access_token;

        header('Location: ?access=' . $accessToken);
        exit(0);
    }

    private function getRequestToken()
    {
        $params = array_merge(
            $this->api->defaultParams(),
            [
                'response_type' => 'code',
                'redirect_uri' => $this->getRedirectURI()
            ]
        );
        header('Location: ' . Config\SlackApiUrls::authorizeUrl . '?' . http_build_query($params, '', '&'));
        exit(0);
    }
}
