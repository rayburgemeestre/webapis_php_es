<?php
namespace CppSe\Factory;

class ElasticSearch implements FactoryInterface
{
    /**
     * @return \Elasticsearch\Client
     */
    public static function create()
    {
        $elasticSearchClientParams = [
            'hosts' => [
                'host' => \CppSe\Config\ElasticSearch::host,
                'port' => \CppSe\Config\ElasticSearch::port
            ]
        ];

        return new \Elasticsearch\Client($elasticSearchClientParams);
    }
}

