<?php
namespace CppSe\Factory;

interface FactoryInterface
{
    public static function create();
}
