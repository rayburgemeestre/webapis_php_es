<?php
namespace CppSe\Utils;

interface ReturnFilter
{
    public function filter($input);
}
