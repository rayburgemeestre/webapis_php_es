<?php
/**
 * Created by PhpStorm.
 * User: trigen
 * Date: 22/03/15
 * Time: 11:23
 */

namespace CppSe\Config;

class ElasticSearch
{
    // Elastic search server
    const host = '127.0.0.1';
    const port = 9200;
}