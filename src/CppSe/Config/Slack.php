<?php
namespace CppSe\Config;

class Slack
{
    const oAuthClientID = '__CLIENT_ID_HERE__';
    const oAuthClientSecret = '__CLIENT_SECRET_HERE__'; // never share this

    public static function indexChannels() {
        return [
            '#general'
        ];
    }
}

