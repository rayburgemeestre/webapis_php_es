<?php
namespace CppSe\DDD;

class ValueObject
{
    const undefined = '1957fbf9-c3f9-49f1-9605-9568eca2ddff';

    private $values = [];

    /**
     * @param ValueObject $instance
     * @param array $constructorArgs
     */
    public function bind($instance, array $constructorArgs)
    {
        $class       = new \ReflectionClass($this);
        $constructor = $class->getConstructor();
        $index       = 0;

        foreach ($constructor->getParameters() as $param) {
            $name = $param->getName();
            $this->values[$name] = null;
            $instance->$name($constructorArgs[$index++]);
        }
    }

    /**
     * @param $value
     * @return null
     */
    protected function accessor($name, $value)
    {
        if ($value === self::undefined)
            return $this->values[$name];

        $this->values[$name] = $value;
    }
}
