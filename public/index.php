<?php
ini_set('include_path', (__DIR__ . '/../') . PATH_SEPARATOR . ini_get('include_path'));
include_once("vendor/autoload.php");

// bootstrap

print "<h1>Checking if curl is installed..</h1>\n";

if (!function_exists('curl_init')) {
    throw new \RuntimeException('curl module not found (function does not exist: curl_init)');
}

print "<h1>Checking if ElasticSearch is available..</h1>\n";

$client = \CppSe\Factory\ElasticSearch::create();
print "<PRE>";
print_r($client->info());
print "</PRE>";

?>

Bootstrap OK, continue:

<h1>Example programs</h1>

<ul>
    <li><a href="./slack/help.php">Slack synchronization</a></li>
    <li><a href="./slack/search.php">Slack search in messages</a></li>
    <li><a href="./meetup/help.php">Meetup.com as OAuth User Auth</a></li>
    <li><a href="./strace/index.php">Strace output visualizer</a></li>
</ul>

