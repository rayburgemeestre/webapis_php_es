<?php
ini_set('include_path', (__DIR__ . '/../../') . PATH_SEPARATOR . ini_get('include_path'));
include_once("vendor/autoload.php");

$elasticSearchClient = \CppSe\Factory\ElasticSearch::create();

$requestId = '0739b3a0-830f-4899-8210-fead2649ef74'; // just hardcode a single request for now..
//$elasticSearchClient->search('')
try {
$queryResponse = $elasticSearchClient->search([
        'index' => 'strace_index',
        'type' => 'strace_line',
        'size' => 100000,
        'body'  => [
//            'query' => [                           // use this if you are not working with a hardcoded request
//                'filtered' => [
//                    'filter' => ['term' => [ 'request_id.raw' => $requestId ]],
//                    //'query' => ['range' => [ 'ts' => [ 'lt' => $timestampSource]]]
//                ],
//            ],
            'sort' => ['@sequence' => [ 'order' => 'asc']]
        ],
    ]);

}
catch (Exception $e) { print_r($e); die; }
print "<PRE>";
$buffer  = [];
$threads = [];

$minTime = null;
$maxTime = null;
foreach ($queryResponse['hits']['hits'] as $hit) {
    $source   = $hit['_source'];
    $stream   = isset($source['stream'])     ? $source['stream']     : null;
    $pid      = isset($source['pid_number']) ? $source['pid_number'] : null;
    $function = isset($source['function'])   ? $source['function']   : null;
    if (!$stream) continue;

    $key = $pid . '_' . $stream;

    list($sec, $msec)   = explode('.', $source['_id']);
    $theTime = ($sec * 1000000) + $msec;
    if ($minTime === null || $theTime < $minTime) $minTime = $theTime;
    if ($maxTime === null || $theTime > $maxTime) $maxTime = $theTime;

    if (in_array($function, ['open', 'socket'])) {
        $buffer[$key] = array();
    }
    if (isset($buffer[$key])) {
        $buffer[$key][] = $source;
    }
    if (isset($buffer[$key]) && in_array($function, ['close'])) {
        $first = $buffer[$key][0];
        $last  = $buffer[$key][count($buffer[$key]) - 1];

        list($sec, $msec)   = explode('.', $first['_id']);
        list($sec2, $msec2) = explode('.', $last['_id']);
        $secs  = $sec2 - $sec;
        $msecs = $msec2 - $msec;
        $diff = ($secs * 1000000) + $msecs;

        $keyX = sprintf('%020.05f', $diff);

        $threads[ $keyX ] = $buffer[$key];
        unset($buffer[$key]);
    }
}

foreach ($buffer as $key => $lines) {
    $lines[] = '*** UNFINISHED ***';
    $threads[$key] = $lines;
}

krsort($threads, SORT_NUMERIC);

$totalDiffTime = $maxTime - $minTime;
print "<PRE><h1>Visualization total time = " . ($totalDiffTime / 1000). " milliseconds</h1>";
$chars = 1000;
$visualizationTable = [];
foreach ($threads as $diff => $lines) {
    if (!isset($lines[0]) || !isset($lines[count($lines) - 1]))
        continue;
    list($sec, $msec) = explode('.', $lines[0]['_id']);
    $firstTime = ($sec * 1000000) + $msec;
    list($sec, $msec) = explode('.', $lines[count($lines) - 1]['_id']);
    $lastTime = ($sec * 1000000) + $msec;

    $firstTimeD = $firstTime - $minTime;
    $lastTimeD = $lastTime - $minTime;

    $firstTimeD = ($firstTimeD / $totalDiffTime) * $chars;
    $lastTimeD = ($lastTimeD / $totalDiffTime) * $chars;

    $s = '';
    $s .= '<div style="float: left; width: ' . $firstTimeD. 'px; overflow: hidden; background-color: grey;">&nbsp;</div>';
    $s .= '<div style="float: left; width: ' . ($lastTimeD - $firstTimeD). 'px; overflow: hidden; background-color: red;">&nbsp;</div>';
    $s .= '<div style="float: left; width: ' . ($chars - $lastTimeD). 'px; overflow: hidden; background-color: grey;">&nbsp;</div>';
    $s .= '<span> = <a href="#anchor'.$diff.'">' . ($diff / 1000). " milisecond thread</a>.\n";
    $visualizationTable['' . $firstTime] = $s;
}
ksort($visualizationTable);
print implode($visualizationTable);
print "</PRE>";

$maxLines = 28;
foreach ($threads as $diff => $lines) {
    print "<a name=\"anchor$diff\"></a><h1>" . ($diff / 1000.0) . " milliseconds</h1>";

    $counter = 0;
    $collapsed = false;
    foreach ($lines as $line) {
        if ($counter == $maxLines) {
            $collapsed = true;
        }
        if (++$counter == $maxLines) {
            print '<div id="more' .$diff .'">...<br/><a href="javascript: vis(\'' . $diff.'\'); void(0);" style="display:none;">show more</a><div style="display:none;">';
        }
        print htmlspecialchars(isset($line['raw_line']) ? $line['raw_line'] : (string)$line) . "\n";
    }
    if ($counter >= 10) {
        print '</div></div></a>';
        if ($collapsed) {
            print '<script type="text/javascript">';
            print 'document.querySelector("div[id=\"more' . $diff . '\"] a").style.display = ""';
            print '</script>';
        }
    }
}
?>
<script type="text/javascript">
function vis(id) {
    var o = document.querySelector('div[id="more' + id + '"] div');
    o.style.display = o.style.display === '' ? 'none' : '';
}
</script>

<?php
print "</PRE>";
?>
