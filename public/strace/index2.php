<?php
ini_set('include_path', (__DIR__ . '/../../') . PATH_SEPARATOR . ini_get('include_path'));
include_once("vendor/autoload.php");

$elasticSearchClient = \CppSe\Factory\ElasticSearch::create();

$requestId = '0739b3a0-830f-4899-8210-fead2649ef74';

//$elasticSearchClient->search('')
$queryResponse = $elasticSearchClient->search([
        'index' => 'strace_index',
        'type' => 'strace_line',
        'size' => 100000,
        'body'  => [
//            'query' => [
//                'filtered' => [
//                    'filter' => ['term' => [ 'request_id.raw' => $requestId ]],
//                    // 'query' => ['range' => [ 'ts' => [ 'lt' => $timestampSource]]]
//                ],
//            ],
            'sort' => ['@sequence' => [ 'order' => 'asc']]
        ],
    ]);

/*
foreach ($queryResponse['hits']['hits'] as $hit) {
    print $hit['_source']['raw_line'] . "\n";
}
die;
*/

print "<PRE>";
$buffer  = [];
$threads = [];

$streamstack = array();
$tree = array();
$treeptr = &$tree;

//new
$data = array();
$pids = array();
$sequences = array();

foreach ($queryResponse['hits']['hits'] as $hit) {
    $source = $hit['_source'];
    $sequence = isset($source['@sequence'])  ? $source['@sequence'] : null;
    $stream   = isset($source['stream'])     ? $source['stream']     : null;
    $return   = isset($source['return'])     ? $source['return']     : null;
    $pid      = isset($source['pid_number']) ? $source['pid_number'] : null;
    $function = isset($source['function']) ? $source['function'] : null;

    $data[$pid][$sequence] = $source;

    $sequences[$sequence] = $pid;

    if (in_array($function, ['clone'])) {
        $pids[] = array($pid, $return);
    }

    /*
    if (!$stream)
        continue;

    $key = $pid . '_' . $stream;

    if (in_array($function, ['open', 'socket'])) {
        $parent = null;
        if (!empty($streamstack)) {
            $parent = end($streamstack);
            $treeptr[$parent]['parent'] = $tree;
            $treeptr = &$tree[$parent]['children'];
        }

 //       for ($i = 0; $i < count($streamstack); $i++) print "&nbsp;&nbsp;";
//        printf(" opening stream %d , %s\n", $stream, $parent);

        $streamstack[$stream] = $stream;
        $buffer[$key] = array();
        if (!isset($treeptr[$stream])) {
            $treeptr[$stream] = array('stream' => $stream, 'children' => array(), 'parent' => $parent);
        }
    }
    if (isset($buffer[$key])) {
        $buffer[$key][] = $source;
    }
    if (isset($buffer[$key]) && in_array($function, ['close'])) {
        $first = $buffer[$key][0];
        $last  = $buffer[$key][count($buffer[$key]) - 1];

        list($sec, $msec)   = explode('.', $first['_id']);
        list($sec2, $msec2) = explode('.', $last['_id']);

        $secs  = $sec2 - $sec;
        $msecs = $msec2 - $msec;

        $diff = ($secs * 1000000) + $msecs;

        $keyX = sprintf('%020.05f', $diff);

        $threads[ $keyX ] = $buffer[$key];
        unset($buffer[$key]);
  //      for ($i = 0; $i < count($streamstack); $i++) print "&nbsp;&nbsp;";
   //     printf(" closing stream %d \n", $stream);
        //printf("%s\n", implode(", ", $streamstack));
        array_pop($streamstack);
    }
        */
}

print_r($pids);
$firstPid = $pids[0][0]; // mehehahahe 
process($firstPid);
function process($pid, $level = 0) {
    global $pids;
    print str_repeat('    ', $level) . "pid = $pid\n";
    foreach ($pids as $pair) {
        list ($pid1, $pid2) = $pair;
        if ($pid1 === $pid) {
            print str_repeat('    ', $level) . "  --> pid2 = $pid2\n";
            process($pid2, $level + 1);
        }
    }
}
die;

function printTree($tree, $indent = 0) {
    foreach ((array)$tree as $streamId => $data) {
        for ($i=0; $i<$indent; $i++) 
            print "    ";
        printf("stream = %d\n", $streamId);
        printTree($data['children'], $indent + 1);
    }
}
printTree($tree);
die;

foreach ($buffer as $key => $lines) {
    $lines[] = '*** UNFINISHED ***';
    $threads[$key] = $lines;
}

ksort($threads, SORT_NUMERIC);
/*
print_r($threads);
//

ksort($threads, SORT_STRING);

print_r($threads['00000000000594.00000']);
die;
*/


foreach ($threads as $diff => $lines) {

    print "<h1>" . ($diff / 1000.0) . " milliseconds</h1>";

    foreach ($lines as $line)
        print htmlspecialchars(isset($line['raw_line']) ? $line['raw_line'] : (string)$line) . "\n";
}
print "</PRE>";
die;


/*
print "<PRE>";
$threads = [];
foreach ($queryResponse['hits']['hits'] as $hit) {
    $source = $hit['_source'];
    $thread = isset($source['occured_during_thread_id']) ? $source['occured_during_thread_id']  : null;
    $stream = isset($source['stream'])     ? $source['stream']     : null;
    $pid    = isset($source['pid_number']) ? $source['pid_number'] : null;

    if (!$thread)
        continue;

    $key = $pid . $thread;
    $key = $pid . $stream . $thread;

    if (!isset($threads[$key]))
        $threads[$key] = array();

    $threads[$key][] = $source;
}

$print = [];
foreach ($threads as $id => $sources) {
    $first = $sources[0];
    $last  = $sources[count($sources) - 1];

    list($sec, $msec)   = explode('.', $first['_id']);
    list($sec2, $msec2) = explode('.', $last['_id']);

    $secs = $sec2 - $sec;
    $msecs = $msec2 - $msec;

    $diff = ($secs * 1000000) + $msecs;

    foreach ($sources as $source)
        $print[ $diff ][] = $source['stream'] . "#" . $source['thread_id'] . "#" . $source['raw_line'];
}

krsort($print);


foreach ($print as $diff => $lines) {
    print "<h1>" . ($diff / 1000.0) . " milliseconds</h1>";
    print_r($lines);
}

print "</PRE>";
*/

//function print_strace_block($streamLinesMap, $streamMetaDataMap)
//{
//    $first = $streamMetaDataMap['first'];
//    $last = $streamMetaDataMap['last'];
//
//    list($sec, $msec)   = explode('.', $first['_id']);
//    list($sec2, $msec2) = explode('.', $last['_id']);
//
//    $secs = $sec2 - $sec;
//    $msecs = $msec2 - $msec;
//
//    $diff = ($secs * 1000000) + $msecs;
//
//    print "<h1>" . $diff . " milliseconds</h1>";
//    print_r($streamLinesMap);
//
//}
