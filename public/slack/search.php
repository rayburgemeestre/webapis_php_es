<?php
ini_set('include_path', (__DIR__ . '/../../') . PATH_SEPARATOR . ini_get('include_path'));
include_once("vendor/autoload.php");

$currentQuery    = $_GET['q'];
$channelSelected = $_GET['channel'];

$elasticSearchClient = \CppSe\Factory\ElasticSearch::create();
$searchService       = new \CppSe\Slack\SlackSearchService($elasticSearchClient);

$users    = $searchService->users();
$channels = $searchService->channels();
$messages = $searchService->messages($currentQuery, $channelSelected);
?>
<style type="text/css">
.message {
    border: solid 1px #c0c0c0;
	margin: 10px;
	padding: 10px;
}
.user {
    font-weight: bold;
	color: red;
}
.highlight {
    background-color: yellow;
}

.message .message {
    margin: 0;
    padding: 0;
    border: none;
}
.message .message .user {
    font-weight: normal;
	color: black;
}
</style>

<form method="GET">
    <input type="text" name="q" value="<?= htmlspecialchars($currentQuery) ?>" />
    <select name="channel">
        <option value="">All channels</option>
        <?php foreach ($channels as $channel): ?>
        <?php if (!in_array('#' . $channel->name(), CppSe\Config\Slack::indexChannels())) continue; ?>
            <option value="<?= htmlspecialchars($channel->id()) ?>"
                <?php if ($channelSelected === $channel->id()): ?>
                    selected="selected"
                <?php endif; ?>
                ><?= htmlspecialchars('#' . $channel->name()) ?></option>
        <?php endforeach; ?>
    </select>
    <input type="submit" />
</form>

<?php
/**@var \CppSe\Slack\ValueObjects\Message[] $messages */
?>
<?php foreach ($messages as $message): ?>

    <div class="message">

        <!-- Three messages that occured before $message (in the same channel), for context -->
        <?php foreach ($searchService->messages_before(strtolower($message->channel()), $message->timestamp()) as $submessage): ?>
            <div class="message">
                <?= message_body($channels, $submessage); ?>
            </div>
        <?php endforeach; ?>

        <!-- The message with the highlighted result -->
        <?= message_body($channels, $message); ?>

    </div>

<?php endforeach; ?>

<?php
function message_body($channels, $message)
{
    $highlightBegin = '<span class="highlight">';
    $highlightEnd   = '</span>';

?>
    <div class="text">
            <span class="user">
                <?= @date('Y-m-d H:i:s', $message->timestamp()) ?>
                &mdash;
                <?= htmlspecialchars('#' . $channels[$message->channel()]->name()) ?>
                &mdash; &lt;<?= htmlspecialchars($message->user()) ?>&gt;
            </span>
        <?php if (!is_array($message->jsonSource()['highlight']['text']) && empty($message->jsonSource()['highlight']['text'])): ?>
            <?= $message->jsonSource()['_source']['text'] ?>
        <?php else: ?>
            <?php foreach ((array)@$message->jsonSource()['highlight']['text'] as $a): ?>
                <?= str_replace(['@@highlight_begin@@', '@@highlight_end@@'], [$highlightBegin, $highlightEnd], htmlspecialchars($a)) ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
<?php
}

