<?php
ini_set('include_path', (__DIR__ . '/../../') . PATH_SEPARATOR . ini_get('include_path'));
include_once("vendor/autoload.php");

// Authenticator will redirect to Slack in order to fetch an Access Token
$slackApi = new \CppSe\Slack\API\SlackApi(CppSe\Config\Slack::oAuthClientID, CppSe\Config\Slack::oAuthClientSecret);
$oauthAuthenticator = new \CppSe\Slack\SlackOAuthService($slackApi);
$oauthAuthenticator->run($_GET);

// OAuth now Authenticated.. show how to install to Cron
if (!isset($_GET['cron'])) {
    show_help($oauthAuthenticator->getRedirectURI(), $slackApi->accessToken());
    exit(0);
}

// The actual synchronization
$slackRepository = new \CppSe\Slack\SlackRepository($slackApi);
$elasticSearchClient = \CppSe\Factory\ElasticSearch::create();
$elasticSearchSynchronizer = new \CppSe\Slack\ElasticSearchSynchronizer($slackRepository, $elasticSearchClient);

if (isset($_GET['verbose']))
    $elasticSearchSynchronizer->setVerbose(true);

$elasticSearchSynchronizer->synchronizeMembers();
$elasticSearchSynchronizer->synchronizeChannels();
$elasticSearchSynchronizer->synchronizeChannelsHistory(CppSe\Config\Slack::indexChannels());

if (isset($_GET['verbose']))
    printf("DONE.\n");

function show_help($redirectURI, $accessToken)
{
    print <<< __HTML__
    <H1>Setup a cron to this URL:</H1>
        <P><pre>{$redirectURI}?access={$accessToken}&amp;cron=true</pre></P>
    <P>You can validate above URL by navigating to it manually and inspect the output.</P>
    <P>You can also add <strong>verbose=true</strong> like this:</P>
    <P><pre>{$redirectURI}?access={$accessToken}&amp;cron=true&amp;verbose=true</pre></P>
__HTML__;
}
