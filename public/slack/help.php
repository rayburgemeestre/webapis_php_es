<h1>OAuth configuration</h1>

<p>Official documentation is located here for Slack: <a href="https://api.slack.com/docs/oauth">https://api.slack.com/docs/oauth</a>.</p>

<h2>Register this application on Slack</h2>

<p>
    Register application here <a href="https://api.slack.com/applications">https://api.slack.com/applications</a>.
</p>

<p>
    If you were to use the current setup, your <strong>application redirect url</strong> will be:

    <script type="text/javascript">
        var url = window.location.href;
        url = url.substring(0, url.lastIndexOf('/') + 1);
        document.write('<PRE>' + url + '</PRE>');
    </script>
</p>

<p>
    Note that you can add multiple Redirect URI's, so you can actually use localhost URL's while developing(!).
</p>

<table>
<tbody>
    <tr>
        <td>
            <h3>Example values:</h3>
            <img src="static/settings.png" width="400">
        </td>
        <td style="vertical-align: top;">
            <h3>Example result:</h3>
            <img src="static/result.png" width="400">
        </td>
    </tr>
</tbody>
</table>

<h2>Configure Slack OAuth in PHP settings</h2>
<p>
    Edit in this project <strong>src/CppSe/Config/Slack.php</strong> with the given client ID &amp; client Secret.
</p>

<p>
    Once done, continue to: <a href="./index.php">here</a>
</p>

<br/>
<br/>
<br/>
