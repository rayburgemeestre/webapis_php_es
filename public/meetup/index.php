<?php
session_start();

if (isset($_SESSION['user']) && isset($userData->id)) {
    header('Location: /ug/poll.php');
    exit(0);
}

if (!isset($_GET['code'])) {
    unset($_SESSION['user']);
    header('Location: https://secure.meetup.com/oauth2/authorize?client_id=__CLIENT_ID_HERE__&response_type=code&redirect_uri=http://cppse.nl/ug/login.php');
    die;
    exit(0);
}

// Redirect provided us the request token
$requestToken = $_GET['code'];

// Now fetch an access token with curl
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://secure.meetup.com/oauth2/access');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => '__CLIENT_ID_HERE__',
            'client_secret' => '__CLIENT_SECRET_HERE__',
            'grant_type' => 'authorization_code',
            'redirect_uri' => 'http://cppse.nl/ug/login.php',
            'code' => $requestToken
        ), '', '&'));
$result = curl_exec($ch);
$access = json_decode($result);
$accessToken = $access->access_token;
curl_close($ch);

// Now fetch the current user
$ch = curl_init();
//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, 'https://api.meetup.com/2/member/self');
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer ' . $accessToken
    ));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($ch);
$userData = json_decode($result);
curl_close($ch);

$_SESSION['user'] = $userData;

header('Location: /ug/poll.php');
exit(0);
